package com.darpa.commstest;

import com.darpa.commstest.CommServiceReporter;
import com.darpa.commstest.OutMessage;

interface CommService {
	void add(CommServiceReporter reporter);
	void remove(CommServiceReporter reporter);
	void sendMessage(in OutMessage outMessage);
}