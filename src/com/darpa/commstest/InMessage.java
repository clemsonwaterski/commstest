package com.darpa.commstest;

import java.util.ArrayList;
import java.util.List;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

public class InMessage implements Parcelable{
			
	private int receivedMessageID;
	private int receivedID;				//other robots id
	private float selfTime;				//my time
	private float receivedTime;			//other robots time aka message id
	private Location receivedLocation;   //other robots location
	private List<Location> receivedObstacleList = new ArrayList<Location>();
	private int receivedLeaderID;		//other robots leader
	
	
	public InMessage(int receivedMessageID, int receivedID, float receivedTime,
			Location receivedLocation, List<Location> receivedObstacleList,
			int receivedLeaderID) {
		super();
		this.receivedMessageID = receivedMessageID;
		this.receivedID = receivedID;
		this.selfTime = System.currentTimeMillis();
		this.receivedTime = receivedTime;
		this.receivedLocation = receivedLocation;
		this.receivedObstacleList = receivedObstacleList;
		this.receivedLeaderID = receivedLeaderID;
	}
	
	public InMessage(int receivedMessageID, int receivedID, float selfTime, float receivedTime,
			Location receivedLocation, List<Location> receivedObstacleList,
			int receivedLeaderID) {
		super();
		this.receivedMessageID = receivedMessageID;
		this.receivedID = receivedID;
		this.selfTime = selfTime;
		this.receivedTime = receivedTime;
		this.receivedLocation = receivedLocation;
		this.receivedObstacleList = receivedObstacleList;
		this.receivedLeaderID = receivedLeaderID;
	}
	
	//Do something with messageID
	
	public int getReceivedMessageID() {
		return receivedMessageID;
	}
	public int getReceivedID() {
		return receivedID;
	}
	public float getReceivedTime() {
		return receivedTime;
	}
	public Location getReceivedLocation() {
		return receivedLocation;
	}
	public List<Location> getReceivedObstacleList() {
		return receivedObstacleList;
	}
	public int getReceivedLeaderID() {
		return receivedLeaderID;
	}
	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(receivedMessageID);
		dest.writeInt(receivedID);
		dest.writeFloat(selfTime);
		dest.writeFloat(receivedTime);
		dest.writeParcelable(receivedLocation,PARCELABLE_WRITE_RETURN_VALUE);
		dest.writeList(receivedObstacleList);
		dest.writeInt(receivedLeaderID);
	}
	
	
	public static Parcelable.Creator<InMessage> CREATOR = new Parcelable.Creator<InMessage>() {

		@Override
		public InMessage createFromParcel(Parcel source) {
			int receivedMessageID = source.readInt();
			int receivedID = source.readInt();
			float selfTime = source.readFloat();
			float receivedTime = source.readFloat();
			Location receivedLocation = source.readParcelable(Location.class.getClassLoader());
			ArrayList<Location> receivedObstacleList = new ArrayList<Location>();
			receivedObstacleList.addAll(source.readArrayList(Location.class.getClassLoader()));
			int receivedLeaderID = source.readInt();
			return new InMessage(receivedMessageID, receivedID, selfTime, receivedTime,
					receivedLocation, receivedObstacleList,
					receivedLeaderID);
		}

		@Override
		public InMessage[] newArray(int size) {
			return new InMessage[size];
		}
		
	};
	
}
