package com.darpa.commstest;


import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.Window;
import android.widget.TextView;

public class TestActivity extends Activity{

	CommServiceManager commManager;
	TextView textView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.activity_main);
        
        textView = (TextView)findViewById(R.id.port);
       	
	}

	
	@Override
    protected void onStart() { 
    	Log.d("Main", "onStart");
    	commManager = new CommServiceManager(this, new CommServiceManager.OnConnectedListener() {
    		@Override public void onConnected() {
    			Log.d("TestActivity", "connected - adding reporter");
    			commManager.add(reporter);
    		}
    		@Override public void onDisconnected() {
    			Log.d("TestActivity", "disconnected - removing reporter");
    			commManager.remove(reporter);
    		}
    	});
    	
    	super.onStart();
    }
    

    @Override
    protected void onStop() {
    	commManager.disconnect();
    	commManager = null;
    	super.onStop();
    }
    
    private CommServiceReporter reporter = new CommServiceReporter.Stub() {
		@Override
		//change string to InMessage
		public void reportInMessage(final String inMessage) throws RemoteException {
			runOnUiThread(new Runnable() {
				@Override public void run() {
					if(inMessage!=null && textView!=null){
						//Log.i("Comms", "in report loop");
						textView.append(inMessage);
					}
				}});
			
		}};
	
	
}
