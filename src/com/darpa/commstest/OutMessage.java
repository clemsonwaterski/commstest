package com.darpa.commstest;

import java.util.ArrayList;
import java.util.List;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

public class OutMessage implements Parcelable {

	private int messageID;
	private int selfID;
	private float selfTime;
	private Location selfLocation;
	private List<Location> obstacleList = new ArrayList<Location>();
	private int leaderID;
	

	//Do something with messageID
	
	
	
	
	public OutMessage(int _selfID, Location _selfLocation,
			ArrayList<Location> _obstacleList, int _leaderID) {
		this.messageID++;
		this.selfID = _selfID;
		this.selfTime = System.currentTimeMillis();
		this.selfLocation = _selfLocation;
		this.obstacleList = _obstacleList;
		this.leaderID = _leaderID;
	}
	
	public void setSelfID(int selfID) {
		this.selfID = selfID;
	}
	public void setSelfTime(float selfTime) {
		this.selfTime = selfTime;
	}
	public void setSelfLocation(Location selfLocation) {
		this.selfLocation = selfLocation;
	}
	public void setObstacleList(List<Location> obstacleList) {
		this.obstacleList = obstacleList;
	}
	public void setLeaderID(int leaderID) {
		this.leaderID = leaderID;
	}
	
	
	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(messageID);
		dest.writeInt(selfID);
		dest.writeFloat(selfTime);
		dest.writeParcelable(selfLocation,PARCELABLE_WRITE_RETURN_VALUE);
		dest.writeList(obstacleList);
		dest.writeInt(leaderID);
	}
	
	public static Parcelable.Creator<OutMessage> CREATOR = new Parcelable.Creator<OutMessage>() {

		@Override
		public OutMessage createFromParcel(Parcel source) {
			int selfID = source.readInt();
			Location selfLocation = source.readParcelable(Location.class.getClassLoader());
			ArrayList<Location> obstacleList = new ArrayList<Location>();
			obstacleList.addAll(source.readArrayList(Location.class.getClassLoader()));
			int leaderID = source.readInt();
			return new OutMessage(selfID, selfLocation, 
					obstacleList, leaderID);
		}

		@Override
		public OutMessage[] newArray(int size) {
			return new OutMessage[size];
		}
		
	};
	
	
}
