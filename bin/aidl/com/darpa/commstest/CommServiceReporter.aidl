package com.darpa.commstest;

import com.darpa.commstest.InMessage;

interface CommServiceReporter {
	void reportInMessage(in String inMessage);
}